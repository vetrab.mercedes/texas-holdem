# Texas hold'em
Demo solution can be found on the “demo_solution” branch.

### Source: there is no external source

### Technology
- Java 8

## Introduction
Card ranking can be found here: https://www.pokerstarsschool.com/lessons/poker-hand-rankings/717/

In this dojo you have to implement a texas hold'em game. Implementation is devided into three user story.
When you create your solution you should run the tests often!

To resolve the first story need to create and implement 
the isValid() method on the HandCategory types and
the calculateCategoryFromCards() method on the HandCategory class and
the constructor of Player class and
the determineHandCategory() method on the TexasHoldemGameImpl class. 

To resolve the second story need to create and implement 
a determineWinners() method on the TexasHoldemGameImpl class. 

To resolve the third story need to create and implement 
a determineWinnersAndMarkHand() method on the TexasHoldemGameImpl class. 

## Stories

#### 1. User story
Run DetermineHandCategoryTest tests often! This story can be a separate dojo.
You have to detect and return the rank of the cards. 
Rank calculation method:
- FOLD: when the last character of the input is an 'f', it means that the player folded his cards.
- High card: no pair, three of a kind, straight, flush, full house, etc., 
then the highest card in your hand is considered to be decisive. 
Like Q♦7♥2♠4♥6♦.
- One pair: If you can form a hand containing two cards of the same value, you have a pair. 
Like 7♦7♥2♠5♥6♦.
- Two pair: If you have two different pairs in your hand, then you have “two pair”. 
Like 7♦7♥5♠5♥6♦.
- Three of a kind: Three cards of the same rank is known as “three of a kind”. 
Like 7♦7♥7♠5♥6♦.
- Straight: A straight consists of five consecutive cards of different suits. 
Like 2♣3♣4♠5♥6♦ OR A♦2♣3♣4♠5♥.
- Flush: A flush consists of five non-consecutive cards of the same suit. 
Like A♣K♣7♣6♣2♣.
- Full house: A full house consists of three of a kind plus a pair. 
- Four of a kind: Four of a kind, consists of four cards of the same rank and one card of another rank. 
Like A♥A♠A♦A♣2♣.
- Straight flush: A straight flush is five consecutive cards of the same suit. 
Like: 2♣3♣4♣5♣6♣ OR A♣2♣3♣4♣5♣.
- Royal flush: A royal flush consists of a straight from ten to the ace with all five cards of the same suit. 
Like: 10♣J♣Q♣K♣A♣.

Input: a string which contains the name of the player and his cards

Output: a string which contains the name of the player and the highest rank of his cards


#### 2. User story
Run DetermineWinnersTest tests often!
Now you can detect the rank of the player's cards, so now you have to determine who is the winner.
Player (more than one if there are more player with the higest rank) who has the highest rank 
will be the winner. You have to mark winner(s) with print "$WINNER$" after them.

Input: List of player names and cards.

Output: List of player names, ranks and the winner mark(s)


#### 3. User story

Run DetermineWinnersAndMarkHandTest tests often!
Now you can detect the rank of the player's cards and the winners, so now you have to detect the hands.
You have to mark the cards which are counted into the hand, with a "*" after the card.

Input: List of player names and cards.

Output: List of player names, cards with hand marks, ranks and the winner mark(s)


## Example
#### 1. User story
```
Input: "Gyuri ba;4s 2c f"
Output: "Gyuri ba:FOLD"
```

#### 2. User story
```
Input: 
Adam;3c Jd Jh 5c 7h Ac Jd
Kevin;Js 3c Ks 10s Qs As Kc
Larry;Ah Kd Ac As 6s Ad 10d
Marco;8s Jc 10c Kd Kc Qc Ac

Output: 
Adam:Three of a kind
Kevin:Royal flush $WINNER$
Larry:Four of a kind
Marco:Royal flush $WINNER$
```

#### 3. User story
```
Input: 
Adam;3c Jd Jh 5c 7h Ac Jd
Kevin;Js 3c Ks 10s Qs As Kc
Larry;Ah Kd Ac As 6s Ad 10d
Marco;8s Jc 10c Kd Kc Qc Ac

Output: 
Adam:5c Kd* Kh* 4d 7h Kc* 2s Three of a kind
Kevin:Js* 3c Ks* 10s* Qs* As* Kc Royal flush $WINNER$
Larry:Ah* Kd Ac* As* 6s Ad* 10d Four of a kind
Marco:8c Jc* 10c* Kd Kc* Qc* Ac* Royal flush $WINNER$
```