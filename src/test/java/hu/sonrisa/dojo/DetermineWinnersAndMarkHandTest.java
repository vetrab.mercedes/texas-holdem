package hu.sonrisa.dojo;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author borosani
 */
public class DetermineWinnersAndMarkHandTest {

    private TexasHoldemGame texasHoldemGame = new TexasHoldemGameImpl();

    @Test
    public void testDetermineWinnersAndMarkHand1() {
        final List<String> playersHands = ImmutableList.<String>builder()
                .add("Adam;5c Kd Kh 4d 7h Kc 2s") // Three of a kind
                .add("Bill;4s 2c f") // Fold
                .add("Courtney;Qc 10h Ah Js 5h Kc 9d") // Straight
                .add("Dick;4d 10d 8s 3h 4c 8s 10c") // Two pair
                .add("Emily;4s 2c Ah 3h 6h 9s f") // Fold
                .add("Frank;7d 10d 6d 9d 6s Ad 8d") // Straight flush
                .add("George;As Ac 10h 10s Qc Ad Qd") // Full house
                .add("Henry;As Ac 10h 2h 4h 9s Qd") // One pair
                .add("Irene;Jd 4d 7d Ad Qd Jh 10d") // Flush
                .add("Jim;4s 2c Ah 3h 6h 9s Jd f") // Fold
                .add("Kevin;Js 3c Ks 10s Qs As Kc") // Royal flush
                .add("Larry;Ah Kd Ac As 6s Ad 10d") // Four of a kind
                .add("Marco;8c Jc 10c Kd Kc Qc Ac") // Royal flush
                .add("Nick;Qc 10h Ah Js 5h Kc Ad") // Straight
                .add("Olga;4c 3d 2h 2d 7s Ac 5d") // Straight
                .add("Peter;7d 10d 6d 9d Jd Kc 8d") // Straight flush
                .build();
        final List<String> result = texasHoldemGame.determineWinnersAndMarkHand(playersHands);
        assertTrue(result.contains("Adam:5c Kd* Kh* 4d 7h Kc* 2s Three of a kind"));
        assertTrue(result.contains("Bill:4s 2c FOLD"));
        assertTrue(result.contains("Courtney:Qc* 10h* Ah* Js* 5h Kc* 9d Straight"));
        assertTrue(result.contains("Dick:4d 10d* 8s* 3h 4c 8s* 10c* Two pair"));
        assertTrue(result.contains("Emily:4s 2c Ah 3h 6h 9s FOLD"));
        assertTrue(result.contains("Frank:7d* 10d* 6d* 9d* 6s Ad 8d* Straight flush"));
        assertTrue(result.contains("George:As* Ac* 10h 10s Qc* Ad* Qd* Full house"));
        assertTrue(result.contains("Henry:As* Ac* 10h 2h 4h 9s Qd One pair"));
        assertTrue(result.contains("Irene:Jd* 4d 7d* Ad* Qd* Jh 10d* Flush"));
        assertTrue(result.contains("Jim:4s 2c Ah 3h 6h 9s Jd FOLD"));
        assertTrue(result.contains("Kevin:Js* 3c Ks* 10s* Qs* As* Kc Royal flush $WINNER$"));
        assertTrue(result.contains("Larry:Ah* Kd Ac* As* 6s Ad* 10d Four of a kind"));
        assertTrue(result.contains("Marco:8c Jc* 10c* Kd Kc* Qc* Ac* Royal flush $WINNER$"));
        assertTrue(result.contains("Nick:Qc* 10h* Ah Js* 5h Kc* Ad* Straight") ||
                result.contains("Nick:Qc* 10h* Ah* Js* 5h Kc* Ad Straight"));
        assertTrue(result.contains("Olga:4c* 3d* 2h* 2d 7s Ac* 5d* Straight") ||
                result.contains("Olga:4c* 3d* 2h* 2d* 7s Ac* 5d* Straight"));
        assertTrue(result.contains("Peter:7d* 10d* 6d 9d* Jd* Kc 8d* Straight flush"));
    }
}
