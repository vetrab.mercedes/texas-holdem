package hu.sonrisa.dojo;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author borosani
 */
public class DetermineWinnersTest {

    private TexasHoldemGame texasHoldemGame = new TexasHoldemGameImpl();

    @Test
    public void testDetermineWinners() {
        final List<String> playersHands = ImmutableList.<String>builder()
                .add("Adam;3c Jd Jh 5c 7h Ac Jd") // Three of a kind
                .add("Bill;4s 2c f") // Fold
                .add("Courtney;Qc 10h Ah Js 5h Kc 9d") // Straight
                .add("Dick;4d Ac 10s 3h 4c 8s 10c") // Two pair
                .add("Emily;4s 2c Ah 3h 6h 9s f") // Fold
                .add("Frank;7d 10d 6d 9d 6s Ad 8d") // Straight flush
                .add("George;As Ac 10h 2h Qc Ad Qd") // Full house
                .add("Henry;As Ac 10h 2h 4h 9s Qd") // One pair
                .add("Irene;Jd 4c 7d Ad Qd Jh 10d") // Flush
                .add("Jim;4s 2c Ah 3h 6h 9s Jd f") // Fold
                .add("Kevin;Js 3c Ks 10s Qs As Kc") // Royal flush
                .add("Larry;Ah Kd Ac As 6s Ad 10d") // Four of a kind
                .add("Marco;8s Jc 10c Kd Kc Qc Ac") // Royal flush
                .build();
        final List<String> expectedResult = ImmutableList.<String>builder()
                .add("Adam:Three of a kind")
                .add("Bill:FOLD")
                .add("Courtney:Straight")
                .add("Dick:Two pair")
                .add("Emily:FOLD")
                .add("Frank:Straight flush")
                .add("George:Full house")
                .add("Henry:One pair")
                .add("Irene:Flush")
                .add("Jim:FOLD")
                .add("Kevin:Royal flush $WINNER$")
                .add("Larry:Four of a kind")
                .add("Marco:Royal flush $WINNER$")
                .build();
        final List<String> result = texasHoldemGame.determineWinners(playersHands);
        assertEquals(expectedResult, result);
    }
}
