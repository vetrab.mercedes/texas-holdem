package hu.sonrisa.dojo;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author borosani
 */
public class DetermineHandCategoryTest {

    private TexasHoldemGame texasHoldemGame = new TexasHoldemGameImpl();

    @Test
    public void testDetermineHandCategoryFold1() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4s 2c f");
        assertEquals("Gyuri ba:FOLD", result);
    }

    @Test
    public void testDetermineHandCategoryFold2() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4s 2c Ah 3h 6h f");
        assertEquals("Gyuri ba:FOLD", result);
    }

    @Test
    public void testDetermineHandCategoryFold3() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4s 2c Ah 3h 6h 9s f");
        assertEquals("Gyuri ba:FOLD", result);
    }

    @Test
    public void testDetermineHandCategoryFold4() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4s 2c Ah 3h 6h 9s Jd f");
        assertEquals("Gyuri ba:FOLD", result);
    }

    @Test
    public void testDetermineHandCategoryHighCard1() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4s 2c Ah 3h 6h 9s Jd");
        assertEquals("Gyuri ba:High card", result);
    }

    @Test
    public void testDetermineHandCategoryHighCard2() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;7c 9h 8h 5s 2h 4c 3d");
        assertEquals("Gyuri ba:High card", result);
    }

    @Test
    public void testDetermineHandCategoryOnePair() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4s 2c Ah 2h 5h 9s Qd");
        assertEquals("Gyuri ba:One pair", result);
    }

    @Test
    public void testDetermineHandCategoryOnePair2() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;As Ac 10h 2h 4h 9s Qd");
        assertEquals("Gyuri ba:One pair", result);
    }

    @Test
    public void testDetermineHandCategoryTwoPair1() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;2d Ac 8s 2h 4h 9s 8c");
        assertEquals("Gyuri ba:Two pair", result);
    }

    @Test
    public void testDetermineHandCategoryTwoPair2() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4d Ac 10s 3h 4c 8s 10c");
        assertEquals("Gyuri ba:Two pair", result);
    }

    @Test
    public void testDetermineHandCategoryThreeOfAKind() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;3c Kd Kh 5c 7h Kc 10d");
        assertEquals("Gyuri ba:Three of a kind", result);
    }

    @Test
    public void testDetermineHandCategoryStraight1() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;8c 10h Ah Js 5h 7c 9d");
        assertEquals("Gyuri ba:Straight", result);
    }

    @Test
    public void testDetermineHandCategoryStraight2() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;Qc 10h Ah Js 5h Kc 9d");
        assertEquals("Gyuri ba:Straight", result);
    }

    @Test
    public void testDetermineHandCategoryStraight3() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;Qc 10h Ah Js 5h Kc 2d");
        assertEquals("Gyuri ba:Straight", result);
    }

    @Test
    public void testDetermineHandCategoryStraight4() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4c 3d 2h 2d 6s Ac 5d");
        assertEquals("Gyuri ba:Straight", result);
    }

    @Test
    public void testDetermineHandCategoryFlush() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;Jd 4c 7d Ad Qd Jh 10d");
        assertEquals("Gyuri ba:Flush", result);
    }

    @Test
    public void testDetermineHandCategoryFullHouse() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;As Ac 10h 2h Qc Ad Qd");
        assertEquals("Gyuri ba:Full house", result);
    }

    @Test
    public void testDetermineHandCategoryFourOfAKind() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;Ah Kd Ac As 6s Ad 10d");
        assertEquals("Gyuri ba:Four of a kind", result);
    }

    @Test
    public void testDetermineHandCategoryStraightFlush1() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;7d 10d 6d 9d 6s Ad 8d");
        assertEquals("Gyuri ba:Straight flush", result);
    }

    @Test
    public void testDetermineHandCategoryStraightFlush2() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;4d 3d 2d 2d 6s Ad 5d");
        assertEquals("Gyuri ba:Straight flush", result);
    }

    @Test
    public void testDetermineHandCategoryRoyalFlush() {
        final String result = texasHoldemGame.determineHandCategory("Gyuri ba;Js 3c Ks 10s Qs As Kc");
        assertEquals("Gyuri ba:Royal flush", result);
    }
}
