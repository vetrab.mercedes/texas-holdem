package hu.sonrisa.dojo.domain;

import com.google.common.collect.Lists;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Defines players in the poker game
 * @author borosani
 */
public class Player {
    /** The name of the player */
    private final String name;

    /**
     * The cards of the player
     * Contains the hole (the cards in the players hand)
     * and board cards (the cards on the board) too.
     */
    private final List<Card> cards;

    /** The category of the players hand (the best five cards of the seven) */
    private HandCategory handCategory;

    public Player(final String name, final String cards) {
        this.name=name;
        //YOU HAVE TO IMPLEMENT THE CARD READING AT THE FIRST STORY! YOU HAVE TO SET THE ISFOLDED PROPERTY HERE!
        this.cards = new ArrayList<>();
    }

    public List<Card> getCards() {
        return CollectionUtils.isEmpty(cards) ? Lists.newArrayList() : cards;
    }

    public String getName() {
        return name;
    }

    public HandCategory getHandCategory() {
        return handCategory;
    }

    public void setHandCategory(HandCategory handCategory) {
        this.handCategory = handCategory;
    }

    public void markHandCards(List<Card> markableCards) {
        this.cards.forEach(card -> {
            if(markableCards.contains(card)){
                card.setInHand(true);
            }
        });
    }
}
