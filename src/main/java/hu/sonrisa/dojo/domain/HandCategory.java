package hu.sonrisa.dojo.domain;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

/**
 * The possibble categories of a hand
 * @author borosani
 */
public enum HandCategory {

    FOLD(0, "FOLD"){
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    ROYAL_FLUSH(10, "Royal flush"){
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    STRAIGHT_FLUSH(9, "Straight flush"){
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    FOUR_OF_A_KIND(8, "Four of a kind") {
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    FULL_HOUSE(7, "Full house"){
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    FLUSH(6, "Flush") {
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    STRAIGHT(5, "Straight"){
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    THREE_OF_A_KIND(4, "Three of a kind") {
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    TWO_PAIR(3, "Two pair") {
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    ONE_PAIR(2, "One pair") {
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    },

    HIGH_CARD(1, "High card") {
        @Override
        public boolean isValid(Collection<Card> cards) {
            //YOU HAVE TO IMPLEMENT THIS FUNCTION AT THE FIRST STORY!
            return false;
        }
    };

    /** The power of the hand category */
    private int rank;

    /** The name of the hand category */
    private String name;

    private HandCategory(final int rank, final String name) {
        this.rank = rank;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getRank() {
        return rank;
    }

    public boolean isValid(Collection<Card> cards) {
        return false;
    }

    public static HandCategory calculateCategoryFromCards(Collection<Card> cards){
        //YOU HAVE TO IMPLEMENT CATEGORY CALCULATION AT THE FIRST STORY!
        return null;
    }

    public static List<Card> getCardsOfCategory(HandCategory handCategory, Collection<Card> cards){
        //YOU HAVE TO IMPLEMENT CARD SELECTING BY CATEGORY AT THE THIRD STORY!
        return new ArrayList<>();
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
