package hu.sonrisa.dojo.domain;

import java.util.Arrays;

/**
 * The possibble suits of a {@link Card}
 * @author borosani
 */
public enum Suit {

    CLUBS('c'),
    DIAMONDS('d'),
    HEARTS('h'),
    SPADES('s'),
    NOTHING('n');

    private char sign;

    Suit(final char sign) {
        this.sign = sign;
    }

    /**
     * Get a {@link Suit} by a sign
     * @param suitSign
     * @return
     */
    public static Suit suitValue(final char suitSign) {
        return Arrays.stream(Suit.values())
                .filter(suitValue -> suitValue.sign == suitSign)
                .findFirst()
                .get();
    }

    public char getSign() {
        return sign;
    }
}
