package hu.sonrisa.dojo.domain;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author borosani
 */
public class Card {

    /** The sign of a card in the hand of the player */
    private static final String IN_HAND_SIGN = "*";

    /**
     * Contains the powers of the special faces
     */
    public static final Map<String, Integer> FACE_POWER_MAP = ImmutableMap.<String, Integer> builder()
            .put("T", 10)
            .put("J", 11)
            .put("Q", 12)
            .put("K", 13)
            .put("A", 14)
            .build();

    private int power;

    private String face;

    private Suit suit;

    private boolean inHand;

    public Card(final String card) {
        final int cardLength = card.length();
        if(card.equals("f")){
            face = "f";
            power = 0;
            suit = Suit.NOTHING;
        } else {
            face = card.substring(0, cardLength - 1);
            power = calculatePower(face);
            suit = Suit.suitValue(card.charAt(cardLength - 1));
        }
    }

    /**
     * Calculates the power of a card by its face
     *
     * @param face - the face of the examined card
     * @return the power of the card
     */
    private static int calculatePower(final String face) {
        return FACE_POWER_MAP.containsKey(face) ? FACE_POWER_MAP.get(face) : Integer.parseInt(face);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(face).append(suit.getSign()).append(isInHand() ? IN_HAND_SIGN : "");
        return sb.toString();
    }

    public int getPower() {
        return power;
    }

    public void setPower(final int power) {
        if (1 < power && power < 15) {
            this.power = power;
        }
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(final Suit suit) {
        this.suit = suit;
    }

    public boolean isInHand() {
        return inHand;
    }

    public void setInHand(boolean inHand) {
        this.inHand = inHand;
    }

    public String printCard(){
        return  face.equals("f") ? "f" : (face + suit.getSign() + (isInHand() ? IN_HAND_SIGN : ""));
    }
}
