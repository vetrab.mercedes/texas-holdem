package hu.sonrisa.dojo;

import java.util.List;

/**
 * @author borosani
 */
public interface TexasHoldemGame {

    /**
     * Determine the players hand category
     * @param playerData - players name + players cards
     *                   (including the cards in his hand and on the board)
     *                   example: Gyuri ba;4s 2c Ah 3h 6h 9s Jd
     * @return The players name and his hand category
     *          example: Gyuri ba:High card
     */
    String determineHandCategory(String playerData);

    /**
     * Determine the players hand category and the winner
     * @param playersData - players name + players cards
     *                   (including the cards in his hand and on the board)
     *                   example: Gyuri ba;4s 2c Ah 3h 6h 9s Jd
     * @return The players name, his hand category, marking the winner
     *           example: Marco:Royal flush $WINNER$
     */
    List<String> determineWinners(List<String> playersData);

    /**
     * Determine the players hand category, the winner and mark the cards
     * which represents the players best hand
     * @param playersData - players name + players cards
     *                   (including the cards in his hand and on the board)
     *                   example: Gyuri ba;4s 2c Ah 3h 6h 9s Jd
     * @return The players name, his cards (marking the cards in his best hand), marking the winner
     *            example: Kevin:Js* 3c Ks* 10s* Qs* As* Kc Royal flush $WINNER$
     */
    List<String> determineWinnersAndMarkHand(List<String> playersData);
}
