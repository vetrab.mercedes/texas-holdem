package hu.sonrisa.dojo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TexasholdemDojoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TexasholdemDojoApplication.class, args);
	}
}
