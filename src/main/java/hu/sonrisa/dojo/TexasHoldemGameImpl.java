package hu.sonrisa.dojo;

import hu.sonrisa.dojo.domain.Card;
import hu.sonrisa.dojo.domain.HandCategory;
import hu.sonrisa.dojo.domain.Player;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author borosani
 */
public class TexasHoldemGameImpl implements TexasHoldemGame {

    /**
     * {@inheritDoc}
     */
    @Override
    public String determineHandCategory(final String playerData) {
        //YOU HAVE TO IMPLEMENT CATEGORY DETERMINATION AT THE FIRST STORY!
        //YOU HAVE TO IMPLEMENT EVERY isValid() FUNCTION OF HandCategory TYPES!
        //YOU HAVE TO USE AND IMPLEMENT calculateCategoryFromCards() METHOD OF HandCategory CLASS!
        //YOU HAVE TO USE AND IMPLEMENT constructor OF Player CLASS!
        throw new NotImplementedException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> determineWinners(final List<String> playersData) {
        //YOU HAVE TO IMPLEMENT CATEGORY DETERMINATION AT THE SECOND STORY!
        throw new NotImplementedException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> determineWinnersAndMarkHand(final List<String> playersData) {
        //YOU HAVE TO IMPLEMENT CATEGORY DETERMINATION AT THE THIRD STORY!
        throw new NotImplementedException();
    }
}
